﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LogInForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.NumPadEnter = New System.Windows.Forms.Button()
        Me.NumPad0 = New System.Windows.Forms.Button()
        Me.NumPadClear = New System.Windows.Forms.Button()
        Me.NumPad9 = New System.Windows.Forms.Button()
        Me.NumPad8 = New System.Windows.Forms.Button()
        Me.NumPad7 = New System.Windows.Forms.Button()
        Me.NumPad6 = New System.Windows.Forms.Button()
        Me.NumPad5 = New System.Windows.Forms.Button()
        Me.NumPad4 = New System.Windows.Forms.Button()
        Me.NumPad3 = New System.Windows.Forms.Button()
        Me.NumPad2 = New System.Windows.Forms.Button()
        Me.NumPad1 = New System.Windows.Forms.Button()
        Me.userIDBox = New System.Windows.Forms.TextBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NumPadEnter
        '
        Me.NumPadEnter.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPadEnter.Location = New System.Drawing.Point(314, 455)
        Me.NumPadEnter.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPadEnter.Name = "NumPadEnter"
        Me.NumPadEnter.Size = New System.Drawing.Size(76, 64)
        Me.NumPadEnter.TabIndex = 28
        Me.NumPadEnter.Text = "&Enter"
        Me.NumPadEnter.UseVisualStyleBackColor = True
        '
        'NumPad0
        '
        Me.NumPad0.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad0.Location = New System.Drawing.Point(230, 455)
        Me.NumPad0.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad0.Name = "NumPad0"
        Me.NumPad0.Size = New System.Drawing.Size(76, 64)
        Me.NumPad0.TabIndex = 27
        Me.NumPad0.Text = "&0"
        Me.NumPad0.UseVisualStyleBackColor = True
        '
        'NumPadClear
        '
        Me.NumPadClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPadClear.Location = New System.Drawing.Point(146, 455)
        Me.NumPadClear.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPadClear.Name = "NumPadClear"
        Me.NumPadClear.Size = New System.Drawing.Size(76, 64)
        Me.NumPadClear.TabIndex = 26
        Me.NumPadClear.Text = "&Clear"
        Me.NumPadClear.UseVisualStyleBackColor = True
        '
        'NumPad9
        '
        Me.NumPad9.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad9.Location = New System.Drawing.Point(314, 384)
        Me.NumPad9.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad9.Name = "NumPad9"
        Me.NumPad9.Size = New System.Drawing.Size(76, 64)
        Me.NumPad9.TabIndex = 25
        Me.NumPad9.Text = "&9"
        Me.NumPad9.UseVisualStyleBackColor = True
        '
        'NumPad8
        '
        Me.NumPad8.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad8.Location = New System.Drawing.Point(230, 384)
        Me.NumPad8.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad8.Name = "NumPad8"
        Me.NumPad8.Size = New System.Drawing.Size(76, 64)
        Me.NumPad8.TabIndex = 24
        Me.NumPad8.Text = "&8"
        Me.NumPad8.UseVisualStyleBackColor = True
        '
        'NumPad7
        '
        Me.NumPad7.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad7.Location = New System.Drawing.Point(146, 384)
        Me.NumPad7.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad7.Name = "NumPad7"
        Me.NumPad7.Size = New System.Drawing.Size(76, 64)
        Me.NumPad7.TabIndex = 23
        Me.NumPad7.Text = "&7"
        Me.NumPad7.UseVisualStyleBackColor = True
        '
        'NumPad6
        '
        Me.NumPad6.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad6.Location = New System.Drawing.Point(314, 313)
        Me.NumPad6.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad6.Name = "NumPad6"
        Me.NumPad6.Size = New System.Drawing.Size(76, 64)
        Me.NumPad6.TabIndex = 22
        Me.NumPad6.Text = "&6"
        Me.NumPad6.UseVisualStyleBackColor = True
        '
        'NumPad5
        '
        Me.NumPad5.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad5.Location = New System.Drawing.Point(230, 313)
        Me.NumPad5.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad5.Name = "NumPad5"
        Me.NumPad5.Size = New System.Drawing.Size(76, 64)
        Me.NumPad5.TabIndex = 21
        Me.NumPad5.Text = "&5"
        Me.NumPad5.UseVisualStyleBackColor = True
        '
        'NumPad4
        '
        Me.NumPad4.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad4.Location = New System.Drawing.Point(146, 313)
        Me.NumPad4.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad4.Name = "NumPad4"
        Me.NumPad4.Size = New System.Drawing.Size(76, 64)
        Me.NumPad4.TabIndex = 20
        Me.NumPad4.Text = "&4"
        Me.NumPad4.UseVisualStyleBackColor = True
        '
        'NumPad3
        '
        Me.NumPad3.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad3.Location = New System.Drawing.Point(314, 241)
        Me.NumPad3.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad3.Name = "NumPad3"
        Me.NumPad3.Size = New System.Drawing.Size(76, 64)
        Me.NumPad3.TabIndex = 19
        Me.NumPad3.Text = "&3"
        Me.NumPad3.UseVisualStyleBackColor = True
        '
        'NumPad2
        '
        Me.NumPad2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad2.Location = New System.Drawing.Point(230, 241)
        Me.NumPad2.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad2.Name = "NumPad2"
        Me.NumPad2.Size = New System.Drawing.Size(76, 64)
        Me.NumPad2.TabIndex = 18
        Me.NumPad2.Text = "&2"
        Me.NumPad2.UseVisualStyleBackColor = True
        '
        'NumPad1
        '
        Me.NumPad1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumPad1.Location = New System.Drawing.Point(146, 241)
        Me.NumPad1.Margin = New System.Windows.Forms.Padding(4)
        Me.NumPad1.Name = "NumPad1"
        Me.NumPad1.Size = New System.Drawing.Size(76, 64)
        Me.NumPad1.TabIndex = 17
        Me.NumPad1.Text = "&1"
        Me.NumPad1.UseVisualStyleBackColor = True
        '
        'userIDBox
        '
        Me.userIDBox.BackColor = System.Drawing.SystemColors.Control
        Me.userIDBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.userIDBox.Location = New System.Drawing.Point(194, 169)
        Me.userIDBox.Margin = New System.Windows.Forms.Padding(4)
        Me.userIDBox.MaxLength = 6
        Me.userIDBox.Name = "userIDBox"
        Me.userIDBox.Size = New System.Drawing.Size(155, 36)
        Me.userIDBox.TabIndex = 16
        Me.userIDBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.userIDBox.UseSystemPasswordChar = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(94, 26)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(380, 118)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 15
        Me.PictureBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(443, 476)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 64)
        Me.Button1.TabIndex = 29
        Me.Button1.Text = "Exit"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'LogInForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.ClientSize = New System.Drawing.Size(552, 553)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.NumPadEnter)
        Me.Controls.Add(Me.NumPad0)
        Me.Controls.Add(Me.NumPadClear)
        Me.Controls.Add(Me.NumPad9)
        Me.Controls.Add(Me.NumPad8)
        Me.Controls.Add(Me.NumPad7)
        Me.Controls.Add(Me.NumPad6)
        Me.Controls.Add(Me.NumPad5)
        Me.Controls.Add(Me.NumPad4)
        Me.Controls.Add(Me.NumPad3)
        Me.Controls.Add(Me.NumPad2)
        Me.Controls.Add(Me.NumPad1)
        Me.Controls.Add(Me.userIDBox)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "LogInForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NumPadEnter As System.Windows.Forms.Button
    Friend WithEvents NumPad0 As System.Windows.Forms.Button
    Friend WithEvents NumPadClear As System.Windows.Forms.Button
    Friend WithEvents NumPad9 As System.Windows.Forms.Button
    Friend WithEvents NumPad8 As System.Windows.Forms.Button
    Friend WithEvents NumPad7 As System.Windows.Forms.Button
    Friend WithEvents NumPad6 As System.Windows.Forms.Button
    Friend WithEvents NumPad5 As System.Windows.Forms.Button
    Friend WithEvents NumPad4 As System.Windows.Forms.Button
    Friend WithEvents NumPad3 As System.Windows.Forms.Button
    Friend WithEvents NumPad2 As System.Windows.Forms.Button
    Friend WithEvents NumPad1 As System.Windows.Forms.Button
    Friend WithEvents userIDBox As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class

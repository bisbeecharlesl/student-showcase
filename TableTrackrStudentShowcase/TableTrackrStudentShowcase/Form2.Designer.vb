﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HostForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DeleteReservationButton = New System.Windows.Forms.Button()
        Me.EditReservationsButton = New System.Windows.Forms.Button()
        Me.AddReservationButton = New System.Windows.Forms.Button()
        Me.ReservationTitleLabel = New System.Windows.Forms.Label()
        Me.LogOutButton = New System.Windows.Forms.Button()
        Me.OverallEstimatedWaitLabel = New System.Windows.Forms.Label()
        Me.OverallTablesAvailableLabel = New System.Windows.Forms.Label()
        Me.OverallTablesOccupiedLabel = New System.Windows.Forms.Label()
        Me.OverallTotalTablesLabel = New System.Windows.Forms.Label()
        Me.SectionEstimatedWaitLabel = New System.Windows.Forms.Label()
        Me.SectionTablesAvailableLabel = New System.Windows.Forms.Label()
        Me.SectionTablesOccupiedLabel = New System.Windows.Forms.Label()
        Me.SectionTotalTablesLabel = New System.Windows.Forms.Label()
        Me.SectionInfoTitleLabel = New System.Windows.Forms.Label()
        Me.PartySizeListBox = New System.Windows.Forms.ListBox()
        Me.DateTimeLabel = New System.Windows.Forms.Label()
        Me.CurrentEmployeeLabel = New System.Windows.Forms.Label()
        Me.SectionComboBox = New System.Windows.Forms.ComboBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.DeleteReservationButton)
        Me.Panel1.Controls.Add(Me.EditReservationsButton)
        Me.Panel1.Controls.Add(Me.AddReservationButton)
        Me.Panel1.Controls.Add(Me.ReservationTitleLabel)
        Me.Panel1.Controls.Add(Me.LogOutButton)
        Me.Panel1.Controls.Add(Me.OverallEstimatedWaitLabel)
        Me.Panel1.Controls.Add(Me.OverallTablesAvailableLabel)
        Me.Panel1.Controls.Add(Me.OverallTablesOccupiedLabel)
        Me.Panel1.Controls.Add(Me.OverallTotalTablesLabel)
        Me.Panel1.Controls.Add(Me.SectionEstimatedWaitLabel)
        Me.Panel1.Controls.Add(Me.SectionTablesAvailableLabel)
        Me.Panel1.Controls.Add(Me.SectionTablesOccupiedLabel)
        Me.Panel1.Controls.Add(Me.SectionTotalTablesLabel)
        Me.Panel1.Controls.Add(Me.SectionInfoTitleLabel)
        Me.Panel1.Controls.Add(Me.PartySizeListBox)
        Me.Panel1.Controls.Add(Me.DateTimeLabel)
        Me.Panel1.Location = New System.Drawing.Point(1027, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(321, 780)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Location = New System.Drawing.Point(-3, 477)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(328, 17)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "________________________________________"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(-3, 325)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(328, 17)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "________________________________________"
        '
        'DeleteReservationButton
        '
        Me.DeleteReservationButton.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.DeleteReservationButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.DeleteReservationButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DeleteReservationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.DeleteReservationButton.Location = New System.Drawing.Point(77, 644)
        Me.DeleteReservationButton.Name = "DeleteReservationButton"
        Me.DeleteReservationButton.Size = New System.Drawing.Size(168, 50)
        Me.DeleteReservationButton.TabIndex = 16
        Me.DeleteReservationButton.Text = "Delete"
        Me.DeleteReservationButton.UseVisualStyleBackColor = False
        '
        'EditReservationsButton
        '
        Me.EditReservationsButton.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EditReservationsButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.EditReservationsButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditReservationsButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.EditReservationsButton.Location = New System.Drawing.Point(77, 588)
        Me.EditReservationsButton.Name = "EditReservationsButton"
        Me.EditReservationsButton.Size = New System.Drawing.Size(168, 50)
        Me.EditReservationsButton.TabIndex = 15
        Me.EditReservationsButton.Text = "Edit"
        Me.EditReservationsButton.UseVisualStyleBackColor = False
        '
        'AddReservationButton
        '
        Me.AddReservationButton.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.AddReservationButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.AddReservationButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddReservationButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AddReservationButton.Location = New System.Drawing.Point(77, 532)
        Me.AddReservationButton.Name = "AddReservationButton"
        Me.AddReservationButton.Size = New System.Drawing.Size(168, 50)
        Me.AddReservationButton.TabIndex = 14
        Me.AddReservationButton.Text = "Add"
        Me.AddReservationButton.UseVisualStyleBackColor = False
        '
        'ReservationTitleLabel
        '
        Me.ReservationTitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReservationTitleLabel.Location = New System.Drawing.Point(3, 494)
        Me.ReservationTitleLabel.Name = "ReservationTitleLabel"
        Me.ReservationTitleLabel.Size = New System.Drawing.Size(318, 35)
        Me.ReservationTitleLabel.TabIndex = 12
        Me.ReservationTitleLabel.Text = "Reservations"
        Me.ReservationTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'LogOutButton
        '
        Me.LogOutButton.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LogOutButton.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.LogOutButton.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LogOutButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.LogOutButton.Location = New System.Drawing.Point(0, 730)
        Me.LogOutButton.Name = "LogOutButton"
        Me.LogOutButton.Size = New System.Drawing.Size(321, 50)
        Me.LogOutButton.TabIndex = 11
        Me.LogOutButton.Text = "Log Out"
        Me.LogOutButton.UseVisualStyleBackColor = False
        '
        'OverallEstimatedWaitLabel
        '
        Me.OverallEstimatedWaitLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OverallEstimatedWaitLabel.Location = New System.Drawing.Point(0, 448)
        Me.OverallEstimatedWaitLabel.Name = "OverallEstimatedWaitLabel"
        Me.OverallEstimatedWaitLabel.Size = New System.Drawing.Size(318, 35)
        Me.OverallEstimatedWaitLabel.TabIndex = 10
        Me.OverallEstimatedWaitLabel.Text = "Overall Estimated Wait"
        Me.OverallEstimatedWaitLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'OverallTablesAvailableLabel
        '
        Me.OverallTablesAvailableLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OverallTablesAvailableLabel.Location = New System.Drawing.Point(0, 406)
        Me.OverallTablesAvailableLabel.Name = "OverallTablesAvailableLabel"
        Me.OverallTablesAvailableLabel.Size = New System.Drawing.Size(318, 32)
        Me.OverallTablesAvailableLabel.TabIndex = 9
        Me.OverallTablesAvailableLabel.Text = "Overall Tables Available"
        Me.OverallTablesAvailableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'OverallTablesOccupiedLabel
        '
        Me.OverallTablesOccupiedLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OverallTablesOccupiedLabel.Location = New System.Drawing.Point(0, 374)
        Me.OverallTablesOccupiedLabel.Name = "OverallTablesOccupiedLabel"
        Me.OverallTablesOccupiedLabel.Size = New System.Drawing.Size(318, 32)
        Me.OverallTablesOccupiedLabel.TabIndex = 8
        Me.OverallTablesOccupiedLabel.Text = "Overall Tables Occupied"
        Me.OverallTablesOccupiedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'OverallTotalTablesLabel
        '
        Me.OverallTotalTablesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OverallTotalTablesLabel.Location = New System.Drawing.Point(0, 342)
        Me.OverallTotalTablesLabel.Name = "OverallTotalTablesLabel"
        Me.OverallTotalTablesLabel.Size = New System.Drawing.Size(318, 32)
        Me.OverallTotalTablesLabel.TabIndex = 7
        Me.OverallTotalTablesLabel.Text = "Overall Total Tables"
        Me.OverallTotalTablesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SectionEstimatedWaitLabel
        '
        Me.SectionEstimatedWaitLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectionEstimatedWaitLabel.Location = New System.Drawing.Point(3, 298)
        Me.SectionEstimatedWaitLabel.Name = "SectionEstimatedWaitLabel"
        Me.SectionEstimatedWaitLabel.Size = New System.Drawing.Size(318, 35)
        Me.SectionEstimatedWaitLabel.TabIndex = 6
        Me.SectionEstimatedWaitLabel.Text = "Estimated Wait"
        Me.SectionEstimatedWaitLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'SectionTablesAvailableLabel
        '
        Me.SectionTablesAvailableLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectionTablesAvailableLabel.Location = New System.Drawing.Point(0, 144)
        Me.SectionTablesAvailableLabel.Name = "SectionTablesAvailableLabel"
        Me.SectionTablesAvailableLabel.Size = New System.Drawing.Size(318, 32)
        Me.SectionTablesAvailableLabel.TabIndex = 5
        Me.SectionTablesAvailableLabel.Text = "Tables Available"
        Me.SectionTablesAvailableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SectionTablesOccupiedLabel
        '
        Me.SectionTablesOccupiedLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectionTablesOccupiedLabel.Location = New System.Drawing.Point(0, 112)
        Me.SectionTablesOccupiedLabel.Name = "SectionTablesOccupiedLabel"
        Me.SectionTablesOccupiedLabel.Size = New System.Drawing.Size(318, 32)
        Me.SectionTablesOccupiedLabel.TabIndex = 4
        Me.SectionTablesOccupiedLabel.Text = "Tables Occupied"
        Me.SectionTablesOccupiedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SectionTotalTablesLabel
        '
        Me.SectionTotalTablesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectionTotalTablesLabel.Location = New System.Drawing.Point(0, 80)
        Me.SectionTotalTablesLabel.Name = "SectionTotalTablesLabel"
        Me.SectionTotalTablesLabel.Size = New System.Drawing.Size(318, 32)
        Me.SectionTotalTablesLabel.TabIndex = 3
        Me.SectionTotalTablesLabel.Text = "Total Tables"
        Me.SectionTotalTablesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SectionInfoTitleLabel
        '
        Me.SectionInfoTitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 16.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectionInfoTitleLabel.Location = New System.Drawing.Point(3, 45)
        Me.SectionInfoTitleLabel.Name = "SectionInfoTitleLabel"
        Me.SectionInfoTitleLabel.Size = New System.Drawing.Size(318, 35)
        Me.SectionInfoTitleLabel.TabIndex = 2
        Me.SectionInfoTitleLabel.Text = "Current Section Name"
        Me.SectionInfoTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PartySizeListBox
        '
        Me.PartySizeListBox.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.PartySizeListBox.FormattingEnabled = True
        Me.PartySizeListBox.ItemHeight = 16
        Me.PartySizeListBox.Location = New System.Drawing.Point(55, 179)
        Me.PartySizeListBox.Name = "PartySizeListBox"
        Me.PartySizeListBox.Size = New System.Drawing.Size(211, 116)
        Me.PartySizeListBox.TabIndex = 1
        '
        'DateTimeLabel
        '
        Me.DateTimeLabel.BackColor = System.Drawing.SystemColors.ControlLight
        Me.DateTimeLabel.Location = New System.Drawing.Point(0, 0)
        Me.DateTimeLabel.Name = "DateTimeLabel"
        Me.DateTimeLabel.Size = New System.Drawing.Size(325, 45)
        Me.DateTimeLabel.TabIndex = 0
        Me.DateTimeLabel.Text = "Date and Time Info"
        Me.DateTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CurrentEmployeeLabel
        '
        Me.CurrentEmployeeLabel.BackColor = System.Drawing.SystemColors.ControlLight
        Me.CurrentEmployeeLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CurrentEmployeeLabel.Location = New System.Drawing.Point(0, 0)
        Me.CurrentEmployeeLabel.Name = "CurrentEmployeeLabel"
        Me.CurrentEmployeeLabel.Size = New System.Drawing.Size(219, 23)
        Me.CurrentEmployeeLabel.TabIndex = 1
        Me.CurrentEmployeeLabel.Text = "Current Employee"
        Me.CurrentEmployeeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'SectionComboBox
        '
        Me.SectionComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectionComboBox.FormattingEnabled = True
        Me.SectionComboBox.Items.AddRange(New Object() {"Main Dining Area", "Bar", "Patio"})
        Me.SectionComboBox.Location = New System.Drawing.Point(515, 1)
        Me.SectionComboBox.Name = "SectionComboBox"
        Me.SectionComboBox.Size = New System.Drawing.Size(220, 37)
        Me.SectionComboBox.TabIndex = 4
        '
        'HostForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1348, 779)
        Me.Controls.Add(Me.SectionComboBox)
        Me.Controls.Add(Me.CurrentEmployeeLabel)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "HostForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form2"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents CurrentEmployeeLabel As System.Windows.Forms.Label
    Friend WithEvents DateTimeLabel As System.Windows.Forms.Label
    Friend WithEvents SectionTablesAvailableLabel As System.Windows.Forms.Label
    Friend WithEvents SectionTablesOccupiedLabel As System.Windows.Forms.Label
    Friend WithEvents SectionTotalTablesLabel As System.Windows.Forms.Label
    Friend WithEvents SectionInfoTitleLabel As System.Windows.Forms.Label
    Friend WithEvents PartySizeListBox As System.Windows.Forms.ListBox
    Friend WithEvents SectionEstimatedWaitLabel As System.Windows.Forms.Label
    Friend WithEvents OverallEstimatedWaitLabel As System.Windows.Forms.Label
    Friend WithEvents OverallTablesAvailableLabel As System.Windows.Forms.Label
    Friend WithEvents OverallTablesOccupiedLabel As System.Windows.Forms.Label
    Friend WithEvents OverallTotalTablesLabel As System.Windows.Forms.Label
    Friend WithEvents LogOutButton As System.Windows.Forms.Button
    Friend WithEvents DeleteReservationButton As System.Windows.Forms.Button
    Friend WithEvents EditReservationsButton As System.Windows.Forms.Button
    Friend WithEvents AddReservationButton As System.Windows.Forms.Button
    Friend WithEvents ReservationTitleLabel As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents SectionComboBox As System.Windows.Forms.ComboBox
End Class
